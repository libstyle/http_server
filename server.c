/*
 *  server.c by Libor Klocan
 *  - HTTP server for BroadbandTrainingZilina
 *  - BTZ-29
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <time.h>
#include <errno.h>
#include <sys/socket.h>
#include "limits.h"
#include "http.h"
#include "debug.h"
#include "server.h"
#include <fcntl.h>

/*
 * server tls closing func
 */
static void server_tls_close(serv_con_pars_t *serv_con_p)
{
    tls_net_free(&serv_con_p->fd_client);
    tls_net_free(&serv_con_p->fd_server);
    tls_cleanup(&serv_con_p->server_tls_struc);
}

/*
 * client closing func
 */
static void server_tls_client_close(serv_con_pars_t *serv_con_p)
{
    tls_net_close(&serv_con_p->fd_client);
}

/*
 * taking care of child processes
 */
static void server_sigchld_handler(int s)
{
    (void)s; /* quiet unused variable warning */

    /* waitpid() might overwrite errno, so we save and restore it: */
    int saved_errno = errno;

    while(waitpid(-1, NULL, WNOHANG) > 0);

    errno = saved_errno;
}

/*
 * get sockaddr, IPv4 or IPv6:
 */
static void *server_get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

/*
 * parsing input arguments
 */
int server_parse_args(int argc, char **argv, serv_con_pars_t *serv_con_p)
{
    int opt = 0, stat = 0, rv = -1;

    serv_con_p->rootdir = NULL;
    serv_con_p->aport = NULL;
    serv_con_p->server_sec = SERV_SEC_TLS_FALSE;

    while((opt = getopt(argc, argv, ":p:d:e")) != -1)
    {
        switch(opt)
        {
        case ':':
            debug_log(ERROR_M, "missing argument for option: %s\n", argv[optind-1]);
            stat = 1;
            break;

        case 'p':
            serv_con_p->aport = optarg;
            serv_con_p->portnum = atoi(optarg);
            break;

        case 'd':
            serv_con_p->rootdir = optarg;
            break;

        case 'e':
            serv_con_p->server_sec = SERV_SEC_TLS_TRUE;
        break;

        case '?':
            debug_log(ERROR_M, "option: %s is not supported\n", argv[optind-1]);
            stat = 1;
            break;

        default:
            break;
        }
    }
    for (; optind < argc; optind++) /* args which are not parsed */
    {
        debug_log(ERROR_M, "not expected arguments: %s\n", argv[optind]);
        stat = 1;
    }
    /* both argument must be set */
    if (serv_con_p->aport == NULL || serv_con_p->rootdir == NULL)
    {
        printf(
            "port number and root directory arguments is necessary:"
            "-p 'PORTNUM' -d 'ROOTDIR'\n\n");
        printf("                                   "
               "for tls connection:-p 'PORTNUM' -d 'ROOTDIR' -e\n");
        stat = 1;
    }

    if (stat)
        goto Error;

    rv = stat;

Error:
    return rv;
}

/*
 * check to open root dir
 */
int server_dir_is_valid(char *dir)
{
    DIR  *pdir;

    pdir = opendir (dir);

    if (!pdir)
    {
        debug_log(ERROR_M,"cannot open directory '%s'\n", dir);
        return 0;
    }

    closedir(pdir);

    return 1;
}

/*
 * server tls handler
 */
static int server_tls_init(serv_con_pars_t *serv_con_p)
{
    int res;

    res = tls_init(&serv_con_p->server_tls_struc);
    if (res < 0)
        return -1;

    res = tls_bind(&serv_con_p->fd_server, serv_con_p->aport);
    if (res < 0)
        return -1;

    return 0;
}

/*
 * server notls handler
 */
static int server_notls_init(serv_con_pars_t *serv_con_p)
{
    int fd_serv;
    struct addrinfo hints = {0}, *servinfo, *p;
    struct sockaddr_in *addr;
    int res, rv = -1, on = 1;

    char *portnum = serv_con_p->aport;

    hints.ai_family = AF_INET6;/* IPv4 and IPv6 support */
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE; /* use my IP */

    if ((res = getaddrinfo(NULL, portnum, &hints, &servinfo)) != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(res));
        goto Exit;
    }

    /* loop through all the results and bind to the first we can */
    for(p = servinfo; p != NULL; p = p->ai_next)
    {
        if ((fd_serv = socket(p->ai_family, p->ai_socktype,
            p->ai_protocol)) == -1)
        {
            debug_log(ERROR_M,"server: socket failed\n");
            continue;
        }
        /* setsockopt */
        if (setsockopt(fd_serv, SOL_SOCKET, SO_REUSEADDR, &on,
            sizeof(int)) == -1)
        {
            close(fd_serv);
            debug_log(ERROR_M,"server: set sockopt failed\n");
            goto Exit;
        }
        /* bind socket */
        if (bind(fd_serv, p->ai_addr, p->ai_addrlen) == -1)
        {
            close(fd_serv);
            debug_log(ERROR_M,"server: bind failing...\n");
            continue;
        }
       break;
    }
    addr = (struct sockaddr_in *)p->ai_addr;

    debug_log(INFO_M,"server IP addr. = %s\n",
        inet_ntoa((struct in_addr)addr->sin_addr));
    debug_log(INFO_M,"port is: %d\n", (int) ntohs(addr->sin_port));

    if (!p)
    {
        debug_log(ERROR_M,"server: failed to bind\n");
        goto Exit;
    }

    debug_log(INFO_M,"server: bind successful\n");

     /* server listening established */
    if (listen(fd_serv, LSNR_MAX) == -1)
    {
        close(fd_serv);
        goto Exit;
    }

    serv_con_p->fd_server = fd_serv;
    rv = 0;
    debug_log(INFO_M,"server: listener was set\n");
Exit:
    freeaddrinfo(servinfo);
    return rv;
}

/*
 * reap all dead processes
 */
int server_reap_procs(void)
{
    struct sigaction sa;

    sa.sa_handler = server_sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD, &sa, NULL) == -1)
    {
        debug_log(ERROR_M,"server: sigaction error\n");
        return -1;
    }
    return 0;
}

/*
 * tls client accept
 */
static int server_tls_client_accept(serv_con_pars_t *serv_con_p)
{
    serv_con_p->fd_client = tls_accept_client(&serv_con_p->fd_server,
            &serv_con_p->server_tls_struc);

    if (serv_con_p->fd_client < 0)
        return -1;

    return 0;
}

/*
 * tls client accept
 */
static int server_notls_client_accept(serv_con_pars_t *serv_con_p)
{
    char client_addr_str[INET6_ADDRSTRLEN];
    client_con_pars_t c_con_p;

    c_con_p.sin_len = sizeof(struct sockaddr_storage);

     /* acccepting connection from client */
    serv_con_p->fd_client = accept(serv_con_p->fd_server,
        (struct sockaddr*)&c_con_p.client_addr, (socklen_t *)&c_con_p.sin_len);

    if (serv_con_p->fd_client < 0)
    {
        debug_log(ERROR_M,"connection failed with error: %d\n", errno);
        return -1;
    }

    /* put address of client to string */
    inet_ntop(c_con_p.client_addr.ss_family,
        server_get_in_addr((struct sockaddr *)&c_con_p.client_addr),
        client_addr_str, sizeof client_addr_str);

    debug_log(INFO_M,"server: got connection from %s\n", client_addr_str);
    debug_log(INFO_M,"client connection established..\n");

    return 0;
}

/*
 * server error msgs to client handler
 */
static char *server_err_handler(http_resp_msg_t respstat, header_req_t hr)
{
    int i = 0;

    http_resp_stat_t statuses[] = {
        {HTTP_RESP_MSG_BAD, "Bad Request"},
        {HTTP_RESP_MSG_NOGET, "Not Implemented"},
        {HTTP_RESP_MSG_NOFOUND, "Not Found"},
        {0, NULL}
    };

    for (i = 0; statuses[i].text != NULL; i++)
    {
        if(statuses[i].code == respstat)
            return http_err_msg(statuses[i].code, statuses[i].text);
    }

    debug_log(ERROR_M, statuses[i].text);
    return NULL;
}

/*
 * wrapper func for sending data to client Tls or noTls
 */
static int server_write_to_client(serv_con_pars_t *serv_con_p, unsigned char *buf,
    int len)
{
    int res, rv = -1;
    if (serv_con_p->server_sec)
    {
        /* call tls write func */
        res =  tls_write_socket(buf, &serv_con_p->server_tls_struc, len);
        if (res < 0)
            goto Exit;
    }
    else
    {
        /* call noTls write */
        res = write(serv_con_p->fd_client, buf, len);
        if (res < 0)
            goto Exit;
    }

    rv = 0;
Exit:
    return rv;
}

/*
 * sending 200ok header to client
 */
static int server_send_ok_header(serv_con_pars_t *serv_con_p, char *con_t,
    header_req_t *head_r)
{
    char *p;
    int res;

    p = http_ok_msg(con_t, head_r->connect);
    res = server_write_to_client(serv_con_p, (unsigned char*)p, strlen(p));
    if (res < 0)
        return -1;

    return 0;
}

/*
 * converting chunk size to ascii
 */
static char *server_chnklen2asci(int len)
{
    static char asize[ASIZE_MAX];

    memset(asize, 0, sizeof(asize));
    sprintf(asize, "%04lx", (long int)len);

    return asize;
}

/*
 * send one chunks
 * returns num of read data or error if occurred
 */
static int server_send_chunk(serv_con_pars_t *serv_con_p, int fd)
{
    int rdata, chinfosz, sdatasz, res;
    unsigned char cdata[CHUNK_SIZE + CHNK_STRT_SIZE + CHNK_END_SIZE];
    unsigned char buf[CHUNK_SIZE];
    char *asize;

    rdata = read(fd, buf, CHUNK_SIZE);

    if (rdata < 0)
        return -1;

    asize = server_chnklen2asci(rdata);
    strcat(asize, ENDCHUNK);
    chinfosz = strlen(asize);
    memcpy(cdata, asize, chinfosz);
    memcpy(cdata + chinfosz, buf, rdata);
    memcpy(cdata + chinfosz + rdata, ENDCHUNK, strlen(ENDCHUNK));
    sdatasz = chinfosz + rdata + strlen(ENDCHUNK);

    res = server_write_to_client(serv_con_p, cdata, sdatasz);
    if (res < 0)
    {
        debug_log(ERROR_M, "write page to client failed\n");
        return -1;
    }

    return rdata;
}

/*
 * sending the last chunk
 */
static int server_send_lstchunk(serv_con_pars_t *serv_con_p)
{
    int res;

    res = server_write_to_client(serv_con_p,
        (unsigned char*)CHNKLAST, strlen(CHNKLAST));
    if (res < 0)
    {
        debug_log(ERROR_M, "write page to client failed\n");
        return -1;
    }

    return 0;
}

/*
 * sending chunks
 */
static int server_send_chunks(serv_con_pars_t *serv_con_p, int fd)
{
    int rdata;

    do
    {
        rdata = server_send_chunk(serv_con_p, fd);
        if(rdata < 0)
           return -1;
    }while (rdata == CHUNK_SIZE);

    return 0;
}

/*
 * server sends data to client
 */
static int server_send_data_to_client(serv_con_pars_t *serv_con_p, char *fpath,
    header_req_t *head_r)
{
    int res = 0, rv = -1;
    char *con_t;
    int fd;

    debug_log(INFO_M,"path: %s\n", fpath);

    con_t = http_get_contype(fpath);

    res = server_send_ok_header(serv_con_p, con_t, head_r);
    if (res < 0)
        goto Error;

    fd = open(fpath, O_RDONLY);
    if (fd < 0)
        goto Error;
    else
        debug_log(INFO_M,"file-open: %s\n", fpath);

    res = server_send_chunks(serv_con_p, fd);
    if (res < 0)
       goto Error;

    res = server_send_lstchunk(serv_con_p);
    if (res < 0)
        goto Error;

    rv = 0;
Error:
    if (fd > 0)
        close(fd);

    return rv;
}

/*
 * searching double enter in request
 */
static int dcrlf_is_exist(char *data, int len)
{
    int i;

    for (i = 0; i < len; i++)
    {
        if (!memcmp(data + i, DCRLF, sizeof(DCRLF)))
            return 1;
    }
    return 0;
}

/*
 * func for read data from client
 * if data end by CRLF returns 0
 * if any error returns -1
 */
static int server_read_from_client(serv_con_pars_t *serv_con_p, input_data_t *indata)
{
    int res = 0, tlen = 0;
    char tbuf[BUFSZ_MAX] = {0};

    while(!dcrlf_is_exist(indata->data, tlen))
    {
        /* condition tls or not*/
        if(serv_con_p->server_sec)
            res = tls_read(tbuf, sizeof(tbuf), &serv_con_p->server_tls_struc);
        else
            /* read for noTls */
            res = read(serv_con_p->fd_client, tbuf, BUFSZ_MAX);
        /* res = number of read data or -1 in case or error */
        if (res < 0)
            return -1;

        if ((tlen + res) > BUFSZ_MAX)
            indata->data = (char *) realloc(indata->data, tlen + res);

        memcpy(indata->data + tlen, tbuf, res);

        tlen += res;
        /* 0 bytes bytes from client = connection will be closed */
        if(res == 0)
            return 0;
    }
    indata->len = tlen;
    return res;
}

/*
 * building file path
 */
static char *server_build_fpath(char *rootd, char* uri)
{
    static char path[PATH_MAX];

    memset(path, 0, sizeof(path));
    strncpy(path, rootd, PATH_MAX);
    strncat(path, "/", PATH_MAX);
    strncat(path, uri, PATH_MAX);

    return path;
}

/*
 * tls client handling end
 */
static void server_tls_client_handler_end(serv_con_pars_t *serv_con_p)
{
    tls_ssl_close_notify(&serv_con_p->server_tls_struc);
}

/*
 * tls initialization before handling client
 */
static int server_tls_client_handler_init(serv_con_pars_t *serv_con_p)
{
    int res;

     tls_net_close(&serv_con_p->fd_server);

    res = tls_reseed("child", &serv_con_p->server_tls_struc);
    if (res < 0)
        return -1;

    res = tls_ssl_setup(&serv_con_p->fd_client, &serv_con_p->server_tls_struc);
    if (res < 0)
        return -1;

    res = tls_handshake(&serv_con_p->server_tls_struc);
    if (res < 0)
        return -1;

    return 0;
}

/*
 * notls initialization before handling client
 */
static int server_notls_client_handler_init(serv_con_pars_t *serv_con_p)
{
    close(serv_con_p->fd_server);

    return 0;
}

/*
 * server child process handler
 */
static int server_client_handler(serv_con_pars_t *serv_con_p)
{
    char *err_msg;
    char *path;
    int res;
    int keep_alive = HTTP_CONNECT_KEEPALIVE;
    http_pars_stat_t pars_stat = HTTP_PARS_STAT_NOERR;
    http_resp_msg_t r_msg_stat;
    input_data_t indata;
    header_req_t head_req;

    do
    {
        /* alloc for buf pointer to maxsize*/
        indata.data = (char*)calloc(BUFSZ_MAX, sizeof(char));
        res = server_read_from_client(serv_con_p, &indata);

        if (res < 0)
            return -1; /* err = close connection */
        if (!res) /* end of keep alive */
            return 0;

        debug_log(INFO_M, "buffer: %s\n", indata.data); /* print data from client*/

        /* parse http request */
        /* returning status of parsing */
        pars_stat = http_header_req_parser_(indata, &head_req);
        free(indata.data);
        keep_alive = head_req.connect;
        path = server_build_fpath(serv_con_p->rootdir, head_req.uri);
        /* status of respose message */
        /* returning 200, 400, 404, 501 */
        r_msg_stat = http_set_resp_msg(&head_req, pars_stat, path);

        if (r_msg_stat != HTTP_RESP_MSG_OK) /* if any error it will be handled */
        {
            /* prepare error message for client */
            err_msg = server_err_handler(r_msg_stat, head_req);
            if (!err_msg)
                return -1; /* err close connection*/

            /* write err message to client */
            res = server_write_to_client(serv_con_p,
                (unsigned char*)err_msg, strlen(err_msg));

            if (res < 0)
                return -1; /* write err close connection*/
            /* close connection */
            return 0;
        }
        /* print parsed data */
        debug_log(INFO_M, "method: %d uri: %s version: %s\n",
                  head_req.method, head_req.uri, head_req.version);

        res = server_send_data_to_client(serv_con_p, path, &head_req);
        if (res < 0)
            return -1; /* err = close connection */
    }while (keep_alive);

    return 0;
}

/*
 * server main loop for tls connection
 */
int server_main_loop(serv_pars_t *serv_p)
{
    int res = 1, rv = -1;
    int pid;

    while (1)
    {
        /* call the client accept function for given type of server */
        res = serv_p->serv_functs.server_client_accept(&serv_p->serv_con_p);
        debug_log(INFO_M,"fd_client: %d accepted\n", serv_p->serv_con_p.fd_client);

        if (res < 0)
        {
            debug_log(ERROR_M,"accept of client failed!\n");
            continue;
        }

        pid = fork();

        if (pid < 0)
        {
            /* call the client close function for given type of server */
            serv_p->serv_functs.server_client_close(&serv_p->serv_con_p);
            debug_log(ERROR_M,"failed!  fork returned\n");
            continue;
        }

        if (!pid)
        {
            /* call the init handler function for given type of server */
            res = serv_p->serv_functs.server_client_handler_init(&serv_p->serv_con_p);
            if (res < 0)
                goto Exit;

            /* call the client handler function */
            res = server_client_handler(&serv_p->serv_con_p);
            if (res < 0)
                goto Exit;

            /* notify function needed before close client in the case of tls extension */
            if (serv_p->serv_functs.server_client_handler_end != NULL)
                serv_p->serv_functs.server_client_handler_end(&serv_p->serv_con_p);

            break;
        }
        /* close client in parent process */
        serv_p->serv_functs.server_client_close(&serv_p->serv_con_p);
    }
    rv = 0;
Exit:
    return rv;
}

/*
 * notls client close func
 */
static void server_notls_client_close(serv_con_pars_t *serv_con_p)
{
    debug_log(INFO_M,"client closed!\n");
    close(serv_con_p->fd_client);
}

/*
 * notls server close func
 */
static void server_notls_close(serv_con_pars_t *serv_con_p)
{
    debug_log(INFO_M,"server closed!\n");
    close(serv_con_p->fd_server);
}

/*
 * socket initialization
 */
int server_init(serv_pars_t *serv_p)
{
    int res;

    debug_log(INFO_M,"***** BTZ-30 server started ******\n\n\n");
    debug_log(INFO_M,"rootpath: %s\n", serv_p->serv_con_p.rootdir);
    debug_log(INFO_M,"portnum: %s\n", serv_p->serv_con_p.aport);

    if (serv_p->serv_con_p.server_sec == SERV_SEC_TLS_TRUE)
    {
        debug_log(INFO_M,"security: TLS\n");
        serv_p->serv_functs.server_init = &server_tls_init;
        serv_p->serv_functs.server_close = &server_tls_close;
        serv_p->serv_functs.server_client_accept = &server_tls_client_accept;
        serv_p->serv_functs.server_client_handler_init = &server_tls_client_handler_init;
        serv_p->serv_functs.server_client_close = &server_tls_client_close;
        serv_p->serv_functs.server_client_handler_end = &server_tls_client_handler_end;
    }
    else
    {
        debug_log(INFO_M,"security: No\n");
        serv_p->serv_functs.server_init = &server_notls_init;
        serv_p->serv_functs.server_close = &server_notls_close;
        serv_p->serv_functs.server_client_accept = &server_notls_client_accept;
        serv_p->serv_functs.server_client_handler_init = &server_notls_client_handler_init;
        serv_p->serv_functs.server_client_close = &server_notls_client_close;
    }

    /* call init function for given type of server*/
    res = serv_p->serv_functs.server_init(&serv_p->serv_con_p);

    if (res < 0)
        return -1;

    return 0;
}

/*
 * server end
 */
void server_end(serv_pars_t *serv_p)
{
    debug_log(INFO_M,"end process\n");
    serv_p->serv_functs.server_client_close(&serv_p->serv_con_p);
    serv_p->serv_functs.server_close(&serv_p->serv_con_p);
}
