#ifndef DEBUG_H
#define DEBUG_H

enum {
    INFO_M = 1,
    ERROR_M = 2,
};

void debug_log(int msg, const char* fmt,...);

#endif