# http_server
 - simple application of http server
 - build: make
 - usage:
	- for http: ./btz30_server -p [PORT_NUM] -d [DESTINATION_DIR]
	- for https: ./btz30_server -p [PORT_NUM] -d [DESTINATION_DIR] -e
