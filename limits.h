#ifndef LIMITS_H
#define LIMITS_H

#include <limits.h>
/*
* MACROS
*/
#define VER_MAX 16
#define MHD_MAX 16
#define MSIZE_MAX 16

#define BUFSZ_MAX 2048
#define CBUF_MAX 128
#define CNTT_MAX 128
#define CBUF_MAX 128
#define HDKEY_MAX 2048
#define HDVAL_MAX 2048

#define SEC 1000000
#define LSNR_MAX 100
#define TIME_MAX 50
#define ASIZE_MAX 10

#endif
