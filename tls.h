#ifndef TLS_H
#define TLS_H
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/net_sockets.h"

/*
* MACROS
*/
#define SSLSERV "ssl_btz_server"
#define DEBUG_LEVEL 0

typedef struct {

    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_entropy_context entropy;
    mbedtls_ssl_config conf;
    mbedtls_x509_crt srvcert;
    mbedtls_pk_context pkey;

} tls_init_struc_t;

/*
 * FUNC
 */
int tls_init(tls_init_struc_t *tls_init_struc);
int tls_bind(int*fd_serv, char *port);
int tls_accept_client(int *fd_server, tls_init_struc_t *tls_init_struc);
int tls_reseed(char *custom, tls_init_struc_t *tls_init_struc);
int tls_ssl_setup(int *fd_client, tls_init_struc_t *tls_init_struc);
int tls_handshake(tls_init_struc_t *tls_init_struc);
int tls_read(char *buf, int len, tls_init_struc_t *tls_init_struc);
int tls_write_socket(unsigned char *buf, tls_init_struc_t *tls_init_struc, int len);
void tls_ssl_free(mbedtls_ssl_context *ssl_context);
void tls_cleanup(tls_init_struc_t *tls_init_struc);
void tls_ssl_close_notify(tls_init_struc_t *tls_init_struc);
void tls_net_init(int *fd);
void tls_net_free(int *fd);
void tls_net_close(int *fd);
#endif