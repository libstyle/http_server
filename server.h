#ifndef SERV_H
#define SERV_H

#include "tls.h"

/*
 * MACROS
 */

#define ENDCHUNK "\r\n"
#define CHNKLAST "0000\r\n\r\n"
#define CHNK_STRT_SIZE 6
#define CHNK_END_SIZE 2
#define CHUNK_SIZE 1024
#define CHUNK_SIZE_MAX CHNK_STRT_SIZE +CHUNK_SIZE + CHNK_END_SIZE

/*
 * STRUCTS & ENUMS
 */
typedef enum {
    SERV_SEC_TLS_FALSE = 0,
    SERV_SEC_TLS_TRUE = 1,
} serv_sec_t;

typedef struct {
    int portnum;
    char *aport;
    char *rootdir;
    struct sockaddr_storage server_addr;
    int fd_server;
    int fd_client;
    serv_sec_t server_sec;
    tls_init_struc_t server_tls_struc;
} serv_con_pars_t;

typedef struct {
    int (*server_init) (serv_con_pars_t *serv_con_p);
    void (*server_close) (serv_con_pars_t *serv_con_p);
    void (*server_client_close) (serv_con_pars_t *serv_con_p);
    int (*server_client_accept) (serv_con_pars_t *serv_con_p);
    int (*server_client_handler_init) (serv_con_pars_t *serv_con_p);
    void (*server_client_handler_end) (serv_con_pars_t *serv_con_p);
    int (*server_client_handler) (serv_con_pars_t *serv_con_p);
} serv_functs_t;

typedef struct {
    serv_con_pars_t serv_con_p;
    serv_functs_t serv_functs;
} serv_pars_t;

typedef struct {
    struct sockaddr_storage client_addr;
    socklen_t sin_len;
} client_con_pars_t;

/*
 * FUNCS
 */
int server_parse_args(int argc, char **argv, serv_con_pars_t *serv_con_pars);
int server_dir_is_valid(char *dir);
int server_init(serv_pars_t *serv_p);
int server_reap_procs(void);
int server_main_loop(serv_pars_t *serv_p);
void server_end(serv_pars_t *serv_p);

#endif
