#include <string.h>
#include <stdio.h>
#include "mbedtls/config.h"
#include "mbedtls/platform.h"
#include "mbedtls/certs.h"
#include "mbedtls/x509.h"
#include "mbedtls/ssl.h"
#include "mbedtls/timing.h"
#include "mbedtls/debug.h"
#include "tls.h"
#include "debug.h"
#include "limits.h"


static void tls_debug(void *ctx, int level,
    const char *file, int line, const char *str)
{
    ((void)level);

    debug_log(INFO_M, "TLS_DEBUG: %s:%04d: %s\n", file, line, str);
}

/*
 * tls initialization
 */
int tls_init(tls_init_struc_t *tls_init_sruc)
{
    int res = 1, rv = -1;
    const char *pers = SSLSERV;

    mbedtls_ssl_config_init(&tls_init_sruc->conf);
    mbedtls_entropy_init(&tls_init_sruc->entropy);
    mbedtls_pk_init(&tls_init_sruc->pkey);
    mbedtls_x509_crt_init(&tls_init_sruc->srvcert);
    mbedtls_ctr_drbg_init(&tls_init_sruc->ctr_drbg);

    /* initial seeding of the RNG */
    debug_log(INFO_M, "initial seeding of the random generator...\n");

    if((res = mbedtls_ctr_drbg_seed(&tls_init_sruc->ctr_drbg,
        mbedtls_entropy_func, &tls_init_sruc->entropy,
             (const unsigned char *) pers, strlen( pers))) != 0)
    {
        debug_log(ERROR_M, "failed! mbedtls_ctr_drbg_seed returned %d\n", res);
        goto Exit;
    }

    debug_log(INFO_M, "ok\n");

    /* load the certificates and private RSA key */
    debug_log(INFO_M, "loading the server cert. and key...\n");

    /* parsing embedded certificate */
    res = mbedtls_x509_crt_parse(&tls_init_sruc->srvcert,
        (const unsigned char *) mbedtls_test_srv_crt, mbedtls_test_srv_crt_len);
    if( res != 0 )
    {
        debug_log(ERROR_M, "failed! mbedtls_x509_crt_parse returned %d\n", res);
        goto Exit;
    }

    res = mbedtls_x509_crt_parse(&tls_init_sruc->srvcert,
        (const unsigned char *)mbedtls_test_cas_pem, mbedtls_test_cas_pem_len);
    if (res != 0)
    {
        debug_log(ERROR_M, "failed! mbedtls_x509_crt_parse returned %d\n", res);
        goto Exit;
    }

    res =  mbedtls_pk_parse_key(&tls_init_sruc->pkey,
        (const unsigned char *) mbedtls_test_srv_key,
            mbedtls_test_srv_key_len, NULL, 0);

    if( res != 0 )
    {
        debug_log(ERROR_M, "failed! mbedtls_pk_parse_key returned %d\n", res);
        goto Exit;
    }

    debug_log(INFO_M, "ok\n");

    /* prepare SSL configuration */
    debug_log(INFO_M, "configuring SSL...\n");

    if((res = mbedtls_ssl_config_defaults(&tls_init_sruc->conf,
                    MBEDTLS_SSL_IS_SERVER,
                    MBEDTLS_SSL_TRANSPORT_STREAM,
                    MBEDTLS_SSL_PRESET_DEFAULT ) ) != 0)
    {
        debug_log(ERROR_M,
            "failed! mbedtls_ssl_config_defaults returned %d\n", res);
        goto Exit;
    }

    mbedtls_ssl_conf_rng(&tls_init_sruc->conf, mbedtls_ctr_drbg_random, &tls_init_sruc->ctr_drbg);
    mbedtls_debug_set_threshold(DEBUG_LEVEL);
    mbedtls_ssl_conf_dbg(&tls_init_sruc->conf, tls_debug, stdout);

    mbedtls_ssl_conf_ca_chain(&tls_init_sruc->conf, tls_init_sruc->srvcert.next, NULL);
    if((res = mbedtls_ssl_conf_own_cert(&tls_init_sruc->conf, &tls_init_sruc->srvcert, &tls_init_sruc->pkey)) != 0)
    {
        debug_log(ERROR_M,
            "failed! mbedtls_ssl_config_defaults returned %d\n", res);
        goto Exit;
    }

    debug_log(INFO_M, "ok\n");

    rv = 0;

Exit:
    return rv;
}

/*
 * bind tls socket
 */
int tls_bind(int *fd_serv, char *port)
{
    int ret = 1;

    /* setup the listening TCP socket */
    debug_log(INFO_M, "bind on https://localhost:%s ...\n", port);

    if((ret = mbedtls_net_bind((mbedtls_net_context *)fd_serv, NULL, port, MBEDTLS_NET_PROTO_TCP)) != 0)
    {
        debug_log(ERROR_M, "failed!  mbedtls_net_bind returned %d\n", ret);
        return -1;
    }

    debug_log(INFO_M, "ok\n");

    return 0;
}

/*
 * tls client accepting
 */
int tls_accept_client(int *fd_server, tls_init_struc_t *tls_init_struc)
{
    mbedtls_net_context client_fd;
    int rv = -1, res = 1;

    mbedtls_net_init(&client_fd);
    mbedtls_ssl_init(&tls_init_struc->ssl);

    debug_log(INFO_M, "waiting for a remote connection ...\n");

    if ((res = mbedtls_net_accept((mbedtls_net_context*)fd_server, &client_fd,
             NULL, 0, NULL)) != 0)
    {
        debug_log(ERROR_M, "failed!  mbedtls_net_accept returned %d\n");
        return rv;
    }
    rv = *(int*)&client_fd;

    return rv;
}

/*
 * tls reseed func
 */
int tls_reseed(char *custom, tls_init_struc_t *tls_init_struc)
{
    int res = 0;

    if ((res = mbedtls_ctr_drbg_reseed(&tls_init_struc->ctr_drbg,
        (const unsigned char *)custom, strlen(custom))) != 0)
    {
        debug_log(ERROR_M, "failed!  mbedtls_ctr_drbg_reseed returned %d\n", res);
        return -1;;
    }

    return 0;
}

/*
 * tls ssl setup func
 */
int tls_ssl_setup(int *fd_client, tls_init_struc_t *tls_init_struc)
{
    int res = 0;

    if ((res = mbedtls_ssl_setup(&tls_init_struc->ssl,
        &tls_init_struc->conf)) != 0)
    {
        debug_log(ERROR_M,
            "ssl setup failed!  mbedtls_ssl_setup returned %d\n", res);
        return -1;
    }

    mbedtls_ssl_set_bio(&tls_init_struc->ssl, (mbedtls_net_context*)fd_client,
        mbedtls_net_send, mbedtls_net_recv, NULL);

    return 0;
}

/*
 * tls handshake func
 */
int tls_handshake(tls_init_struc_t *tls_init_struc)
{
    int res = 0;

    debug_log(INFO_M, "performing the SSL/TLS handshake.\n");

    while ((res = mbedtls_ssl_handshake(&tls_init_struc->ssl)) != 0)
    {
        if (res != MBEDTLS_ERR_SSL_WANT_READ && res != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            debug_log(ERROR_M,
                "ssl handshake failed!  mbedtls_ssl_handshake returned %d\n", res);
            return -1;
        }
    }
    debug_log(INFO_M, "ssl handshake ok\n");

    return 0;
}

/*
 * tls read socket func
 */
int tls_read(char *buf, int len, tls_init_struc_t *tls_init_struc)
{
    int res = 0;

    debug_log(INFO_M, "start reading from client.\n");
    do
    {
        res = mbedtls_ssl_read(&tls_init_struc->ssl, (unsigned char*)buf, len);

        if (res == MBEDTLS_ERR_SSL_WANT_READ || res == MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            continue;
        }

        if (res < 0)
            return -1;

        if (res == 0 || res > 0)
            break;

    } while (1);

    return res;
}

/*
 * tls write func
 */
int tls_write_socket(unsigned char *buf, tls_init_struc_t *tls_init_struc, int len)
{
    int res = 0;

    while ((res = mbedtls_ssl_write(&tls_init_struc->ssl, buf, len)) <= 0)
    {
        if (res == MBEDTLS_ERR_NET_CONN_RESET)
        {
            debug_log(ERROR_M,
                      "write failed!  peer closed the connection\n", res);
            return -1;
        }

        if (res != MBEDTLS_ERR_SSL_WANT_READ && res != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            debug_log(ERROR_M,
                      "write failed!  mbedtls_ssl_write returned %d\n", res);
            return -1;
        }
    }
    len = res;

    return 0;
}

/*
 * tls close notify
 */
void tls_ssl_close_notify(tls_init_struc_t *tls_init_struc)
{
    mbedtls_ssl_close_notify(&tls_init_struc->ssl);
}

/*
 * net init of server or client fd
 */
void tls_net_init(int *fd)
{
    mbedtls_net_init((mbedtls_net_context*)fd);
}

/*
 * net free of server or client fd
 */
void tls_net_free(int *fd)
{
    mbedtls_net_free((mbedtls_net_context*)fd);
}

/*
 * net free of server or client fd
 */
void tls_net_close(int *fd)
{
    mbedtls_net_close((mbedtls_net_context*)fd);
}

/*
 * clean up of entire tls_ini struct
 */
void tls_cleanup(tls_init_struc_t *tls_init_struc)
{
    mbedtls_x509_crt_free(&tls_init_struc->srvcert);
    mbedtls_pk_free(&tls_init_struc->pkey);
    mbedtls_ssl_free(&tls_init_struc->ssl);
    mbedtls_ssl_config_free(&tls_init_struc->conf);
    mbedtls_ctr_drbg_free( &tls_init_struc->ctr_drbg);
    mbedtls_entropy_free(&tls_init_struc->entropy);
}

/*
 * free ssl func
 */
void tls_ssl_free(mbedtls_ssl_context *ssl_context)
{
    mbedtls_ssl_free(ssl_context);
}

