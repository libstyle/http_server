CC=gcc

TARGET=btz30_server
OBJECTS=main.o server.o http.o debug.o tls.o
LIBS=-lmbedtls -lmbedx509 -lmbedcrypto
DEP=library/libmbedcrypto.a library/libmbedx509.a library/libmbedtls.a
INCLUDES=-Iinclude

$(TARGET): $(OBJECTS) 
	$(CC) $(OBJECTS) -Llibrary $(LIBS) -o $(TARGET)

$(DEP):
	$(MAKE) -C ./library

main.o: main.c
	$(CC) -Wall -g $(INCLUDES) -c main.c

server.o: server.c
	$(CC) -Wall -g $(INCLUDES) -c server.c

http.o: http.c 
	$(CC) -Wall -g -c http.c 

debug.o: debug.c 
	$(CC) -Wall -g -c debug.c 


tls.o: tls.c $(DEP)
	$(CC) -Wall -g $(INCLUDES) -c tls.c 

.PHONY: clean

clean:
	rm *.o btz30_server
	$(MAKE) -C ./library clean
