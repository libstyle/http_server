#ifndef HTTPHEADER_H
#define HTTPHEADER_H

#include "limits.h"

/*
* MACROS
*/
#define METHOD_GET_EXT "GET /"
#define INXDEFPAGE "index.html"
#define DEFPAGE "default.html"
#define HTTP1_0 "HTTP/1.0"
#define HTTP1_1 "HTTP/1.1"
#define KEEPALIVE "keep-alive"
#define CLOSE "close"

#define CRLF "\r\n"
#define DCRLF "\r\n\r\n"

/*
* STRUCTS & ENUMS
*/
typedef enum {
    HTTP_METHOD_NOSUP = 0,
    HTTP_METHOD_GET = 1,
} http_method_t;

typedef enum {
    HTTP_PARS_STAT_NOERR = 0,
    HTTP_PARS_STAT_ERR = 1,
} http_pars_stat_t;

typedef enum {
    HTTP_RESP_MSG_OK = 200,
    HTTP_RESP_MSG_BAD = 400,
    HTTP_RESP_MSG_NOFOUND = 404,
    HTTP_RESP_MSG_NOGET = 501,
} http_resp_msg_t;

typedef enum {
    HTTP_VERSION_UKN = 0,
    HTTP_VERSION_10 = 10,
    HTTP_VERSION_11 = 11,
} http_version_t;

typedef struct server {
    http_resp_msg_t code;
    char *text;
} http_resp_stat_t;

typedef struct {
    char *suff;
    char *type;
} http_content_type_t;

typedef enum {
    HTTP_CONNECT_CLOSE = 0,
    HTTP_CONNECT_KEEPALIVE = 1,
} http_connect_t;

typedef struct {
    int method;
    http_connect_t connect;
    char uri[PATH_MAX];
    char version[VER_MAX];
    http_version_t iversion;
} header_req_t;

typedef struct {
    char *data;
    int len;
} input_data_t;

/*
* FUNC
*/
http_pars_stat_t http_header_req_parser_(input_data_t indata, header_req_t *head_r);
char *http_err_msg(int code_num, char *msg);
char *http_ok_msg(char *con_type, int connection);
http_resp_msg_t http_set_resp_msg(header_req_t *head_r, http_pars_stat_t ps,
    char *path);
char *http_get_contype(char *path);

#endif
