#include <sys/socket.h>
#include "debug.h"
#include "tls.h"
#include "server.h"
#include "limits.h"

int main(int arg, char **argv)
{
    int rv = -1;
    serv_pars_t serv_p = {0};

    if (server_parse_args(arg, argv, &serv_p.serv_con_p) < 0)
        goto Exit;

    /* try open dir */
    if (!server_dir_is_valid(serv_p.serv_con_p.rootdir))
        goto Exit;

    /* server handler */
    if (server_init(&serv_p) < 0)
        goto Exit;

    /* reap child processes */
    if (server_reap_procs() < 0)
        goto Exit;

    /* server main */
    if (server_main_loop(&serv_p) < 0)
        goto Exit;

    rv = 0;

Exit:
    server_end(&serv_p);
    return rv;
}
