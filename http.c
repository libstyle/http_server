#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include "limits.h"
#include "http.h"
#include "debug.h"

/*      */
/* TODO */
/*      */

/*
 * get num of method
 */
static http_method_t http_method_str2enum(char *str)
{
    if (!strncmp(str, METHOD_GET_EXT, strlen(METHOD_GET_EXT)))
        return HTTP_METHOD_GET;
    return HTTP_METHOD_NOSUP;
}

/*
 * search char in buffer with given size
 */
static char *search_char(char* buf, char ch, int len)
{
    int i;
    char *ptr = NULL;

    for (i = 0;i < len; i++)
    {
        ptr = buf + i;
        if (*ptr == ch)
            break;
    }

    return ptr;
}

/*
 * check uri last char for default page
 */
static void http_check_uri_lstchar(char *uri)
{
    char *lch = uri;

    if (strlen(uri) > 0)
        lch = uri + strlen(uri)-1;

    if (!strcmp(lch, "\0"))
    {
        memset(uri, 0, PATH_MAX);
        strcpy(uri, INXDEFPAGE);
    }
    if (!strcmp(lch, "/"))
    {
        memset(uri, 0, PATH_MAX);
        strcpy(uri, " /"); /* page not found */
    }
}

/*
 * parsing method func
 */
static http_pars_stat_t http_parse_method(input_data_t *indata, header_req_t *head_r)
{
    char *tp = NULL, *p;
    int len;

    p = indata->data;
    len = indata->len;
    /* find space after method */
    tp = search_char(p, ' ', len);
    if (!tp)
        return HTTP_PARS_STAT_ERR;

    head_r->method = http_method_str2enum(p);

    indata->data = tp;
    indata->len = len - (tp - p);

    return HTTP_PARS_STAT_NOERR;
}

/*
 * parse and get uri
 */
static http_pars_stat_t http_parse_uri(input_data_t *indata, header_req_t *head_r)
{
    char *tp = NULL,*tq = NULL, *suri;
    int tlen, len;

    tp = indata->data;
    len = indata->len;

    /* if space found shift pointer to start of uri */
    tp += strlen(" /");
    /* store start of uri to temp pointer */
    suri = tp;

    /* searching for space */
    tp = search_char(suri, ' ', len);
    if (tp == NULL) /* if no space after uri = bad request*/
        return HTTP_PARS_STAT_ERR; /* err during parsing */

    /* searching for query */
    tq = search_char(suri, '?', len);

    /* get length of uri */
    if (tq != NULL && tp > tq)  /* if query is before space -> */
        tlen = tq - suri;       /* len = uri start to query */
    else                        /* query is behind space -> */
        tlen = tp - suri;       /* len = uri start to space */

    /* if uri is too large */
    if (tlen > (PATH_MAX-1))
        return HTTP_PARS_STAT_ERR; /* err during parsing */
    /* copy uri */
    strncpy(head_r->uri, suri, tlen);
    http_check_uri_lstchar(head_r->uri);

    /* shift pointer to start of version */
    tlen = tp - suri;
    indata->data = suri + tlen;

    return HTTP_PARS_STAT_NOERR;
}

/*
 * parsing HTTP version
 */
static http_pars_stat_t http_parse_version(input_data_t *indata, header_req_t *head_r)
{
    char *tp = NULL, *vsrt;

    tp = indata->data;
    /* if space found shift pointer to start of HTTP proto version */
    tp += strlen(" ");

    /* check HTTP/ string occurrence right after space behind of uri */
    if (strncmp(tp, "HTTP/", strlen("HTTP/")))
        return HTTP_PARS_STAT_ERR;

    vsrt = tp;
    tp += strlen("HTTP/");

    /* check http version and end of state line */
    head_r->iversion = HTTP_VERSION_11;
    tp = strstr(vsrt, "1.0\r\n");
    if (!tp)
        tp = strstr(vsrt, "1.1\r\n");
    else head_r->iversion = HTTP_VERSION_10;

    if (!tp)
    {
        head_r->iversion = HTTP_VERSION_UKN;
        return HTTP_PARS_STAT_ERR;
    }
    /* for version 1.0 set close */
    if (head_r->iversion == HTTP_VERSION_10)
        head_r->connect = HTTP_CONNECT_CLOSE;

    /* for version 1.1 set keepalive by default */
    if (head_r->iversion == HTTP_VERSION_11)
        head_r->connect = HTTP_CONNECT_KEEPALIVE;

    strncpy(head_r->version, "HTTP/", strlen("HTTP/"));
    strncat(head_r->version, tp, strlen("1.X"));

    /* shift poiter to start of next line of request = start of headers */
    indata->data = tp + strlen("x.x\r\n");

    return HTTP_PARS_STAT_NOERR;
}

/*
 * get len of header line
 */
static int http_header_get_len(char *start)
{
    char *p;

    p = strstr(start, CRLF);
    if (!p)
        return 0;

    return (p - start);
}

/*
 * get key and value from header
 */
static int http_header_get_keyval(char **start, char *key, char *val, int len)
{
    int k = 0, v = 0, space = 0, tinx = 0;

     /* get key loop */
    for (k = 0; k < len; k++)
    {
        if (**start == ':')
           break;
        *(key + k) = *(*start)++;
    }

    /* no key or no colon */
    if (k == 0 || k == len)
        return -1;

    /* jump over colon */
    (*start)++;

    /* get value */
    for (v = k; v < (len-1); v++)
    {
        if (**start == ' ' && !space)
        {
            (*start)++;
            continue;
        }
        else
        {
           space = 1;
           *(val + tinx) = *(*start)++;
           tinx++;
        }
    }

    /* set start to next line */
    (*start) += strlen(CRLF);

    return 0;
}

/*
 * hndl for connection header
 */
static int http_handle_connection_header(char *value, header_req_t *head_r)
{
    if (!strcasecmp(value, KEEPALIVE) && head_r->iversion == HTTP_VERSION_11)
        head_r->connect = HTTP_CONNECT_KEEPALIVE;

    if (!strcasecmp(value, CLOSE))
        head_r->connect = HTTP_CONNECT_CLOSE;

    return 0;
}

/*
 * hndl single header
 */
static int http_handle_header(char *key, char *value, header_req_t *head_r)
{

    if (!strcasecmp(key, "connection"))
       http_handle_connection_header(value, head_r);

    return 0;
}

/*
 * parsing headers
 */
static int http_parse_headers(char *buff, header_req_t *head_r)
{
    char *start = buff;
    char key[HDKEY_MAX], value[HDVAL_MAX];
    int len = 0, res = 0;

    while (1)
    {
        memset(key, 0, sizeof(key));
        memset(value, 0, sizeof(value));

        len = http_header_get_len(start);

        if (!len) /* end of headers */
           break;
        res = http_header_get_keyval(&start, key, value, len);
        /* res == 0 no error; res == -1 no key found -> next iteration */
        if (res < 0)
            continue;

        http_handle_header(key, value, head_r);
    }

    return 0;
}

/*
 * header request parser
 */
http_pars_stat_t http_header_req_parser_(input_data_t indata, header_req_t *head_r)
{
    int res = 0;

    /* header struct set to zero  */
    memset(head_r, 0, sizeof(header_req_t));

    res = http_parse_method(&indata, head_r);
    if (res == HTTP_PARS_STAT_ERR)
        return HTTP_PARS_STAT_ERR;

    debug_log(INFO_M,"Method is: %d\n", head_r->method);

    res = http_parse_uri(&indata, head_r);
    if (res == HTTP_PARS_STAT_ERR)
        return HTTP_PARS_STAT_ERR;

    debug_log(INFO_M,"Uri is: %s\n", head_r->uri);

    res = http_parse_version(&indata, head_r);
    if (res == HTTP_PARS_STAT_ERR)
        return HTTP_PARS_STAT_ERR;

    debug_log(INFO_M,"Ver is: %s\n", head_r->version);

    http_parse_headers(indata.data, head_r);

    debug_log(INFO_M,"Connection: %d\n", head_r->connect);

    return HTTP_PARS_STAT_NOERR;
}

/*
 * get con type of the file
 */
char *http_get_contype(char *path)
{
    int i;
    http_content_type_t contypes[] = {
        {".html", "text/html"},
        {".gif", "text/gif" },
        {".png", "image/png"},
        {".jpg", "image/jpg"},
        {".js", "text/javascript"},
        {".css", "text/css"},
        {".pdf", "application/pdf"},
        {NULL, NULL}
    };

    for (i = 0; contypes[i].suff != NULL; i++)
    {
        if (strstr(path, contypes[i].suff))
            return contypes[i].type;
    }

    return "text/plain";
}

/*
 * client_err_msg - html error response for client
 */
char *http_err_msg(int code_num, char *msg)
{
    static char err_msg[BUFSZ_MAX];

    sprintf(err_msg, "HTTP/1.1 %d %s\r\n",code_num, msg);
    sprintf(err_msg + strlen(err_msg), "Content-type: text/html\r\n");
    sprintf(err_msg + strlen(err_msg), "Connection: close\r\n");
    sprintf(err_msg + strlen(err_msg), "\r\n");
    sprintf(err_msg + strlen(err_msg), "%d: %s\r\n", code_num, msg);

    debug_log(ERROR_M, "http error message: %d %s\n", code_num , msg);

    return err_msg;
}

/*
 * check of existence of file
 */
static int http_file_is_exist(char *path)
{
    FILE *fp;

    fp = fopen(path, "r");
    if(fp)
    {
        fclose(fp);
        return 1;
    }
    return 0;
}

/*
 * set response message for client
 */
http_resp_msg_t http_set_resp_msg(header_req_t *head_r, http_pars_stat_t ps,
    char *path)
{
    if (ps == HTTP_PARS_STAT_ERR)
        return HTTP_RESP_MSG_BAD;

    if (head_r->method != HTTP_METHOD_GET)
        return HTTP_RESP_MSG_NOGET;

    if (!http_file_is_exist(path))
        return HTTP_RESP_MSG_NOFOUND;

    return HTTP_RESP_MSG_OK;
}

/*
 * client_ok_msg - html ok response header for client
 */
char *http_ok_msg(char *con_type, int connection)
{
    static char buf[BUFSZ_MAX];
    char connbuf[CBUF_MAX]={0};

    /* set keep-alive in the case */
    if (connection == HTTP_CONNECT_KEEPALIVE)
        strncpy(connbuf, KEEPALIVE, strlen(KEEPALIVE));

    /* in case connection close set close */
    if (connection == HTTP_CONNECT_CLOSE)
        strncpy(connbuf, CLOSE, strlen(CLOSE));

    /* print response header */
    sprintf(buf, "HTTP/1.1 200 OK\r\n");
    sprintf(buf + strlen(buf), "Connection: %s\r\n", connbuf);
    sprintf(buf + strlen(buf), "Content-type: %s\r\n", con_type);
    sprintf(buf + strlen(buf), "Transfer-Encoding: chunked\r\n");
    sprintf(buf + strlen(buf), "\r\n");
    debug_log(INFO_M, "header response: %s\n", buf);

    return buf;
}

