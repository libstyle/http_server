#include <stdio.h>
#include <time.h>
#include <stdarg.h>
#include "debug.h"
#include "limits.h"

/*      */
/* TODO */
/*      */

/*
 * log level num to string
 */
static char *log_level2str(int errlev)
{
    if (errlev==ERROR_M)
        return "ERROR ";

    if (errlev==INFO_M)
        return "INFO ";

    return "UNKNOW_LEV";
}

/*
 * debug log func for write messages on console
 */
void debug_log(int msg, const char* fmt,...)
{
    char tmsg[TIME_MAX];
    va_list args;

    strftime(tmsg, sizeof tmsg, "%T", localtime(&(time_t){time(NULL)}));
    printf("[%s] ", tmsg);
    printf("%s",log_level2str(msg));
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}
